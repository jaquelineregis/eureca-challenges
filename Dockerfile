FROM python:3.7

ENV PYTHONUNBUFFERED 1

RUN mkdir /code

WORKDIR /code

COPY requirements.txt /code/
RUN pip install -r requirements.txt

COPY docker-app-start.sh /code/
RUN chmod +x docker-app-start.sh

COPY . /code/

EXPOSE 8000
