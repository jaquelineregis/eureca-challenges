from django.views.generic import ListView
from django.views.generic.base import TemplateView

from challenges.models import IMDB
from challenges.utils import get_query, findMissingInteger


class Homepage(TemplateView):
    template_name = "challenges/home.html"


class BigData(TemplateView):
    template_name = "challenges/big_data.html"


class IMDBListView(ListView):
    model = IMDB
    paginate_by = 10
    ordering = ['-num_votes']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['query'] = self.request.GET.get('query', '')
        return context

    def get_queryset(self):
        queryset = super().get_queryset()

        query = self.request.GET.get('query')
        if query:
            queryset = IMDB.objects.filter(primary_title__icontains=query).order_by('-num_votes')

        return queryset


class MissingInteger(TemplateView):
    template_name = "challenges/missing_integer.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['query'] = list_query = get_query(self.request.GET.get('query', ''))
        context['result'] = findMissingInteger(list_query)
        return context
