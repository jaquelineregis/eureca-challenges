import pandas as pd

from django.core.management.base import BaseCommand, CommandError
from sqlalchemy import create_engine


class Command(BaseCommand):
    help = 'Add csv into database'

    def handle(self, *args, **options):
        print('Start download!')
        movies = self.get_csv('basics')
        ratings = self.get_csv('ratings')
        print('Completed download!')

        print('Waiting! We are creating the CSV.')
        movies_ratings = pd.merge(movies, ratings, on='tconst')
        # movies_ratings = movies_ratings[['tconst', 'primaryTitle', 'originalTitle', 'averageRating', 'numVotes']]
        movies_ratings = movies_ratings[['tconst', 'primaryTitle', 'numVotes']]
        # movies_ratings.rename(columns={'primaryTitle': 'primary_title', 'originalTitle': 'original_title',
        #                                'averageRating': 'average_rating', 'numVotes': 'num_votes'}, inplace=True)
        movies_ratings.rename(columns={'primaryTitle': 'primary_title', 'numVotes': 'num_votes'}, inplace=True)
        print('CSV created!')

        print('Waiting! We are import CSV file to a table in PostgreSQL.')
        # engine = create_engine('postgres://postgres:12345@localhost:5432/eureca')
        engine = create_engine('postgres://postgres:12345@db:5432/postgres')
        movies_ratings.to_sql('imdb', engine, if_exists='append', index=False)
        print('Finally, the end! Bye.')

    def get_csv(self, local):
        url = f'https://datasets.imdbws.com/title.{local}.tsv.gz'
        return pd.read_csv(url, compression='gzip', sep='\t')
