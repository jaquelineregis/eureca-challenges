from django.urls import path

from .views import Homepage, BigData, IMDBListView, MissingInteger

urlpatterns = [
    path('', Homepage.as_view(), name='homepage'),
    path('challenge-1', BigData.as_view(), name='big-data'),
    path('challenge-2', IMDBListView.as_view(), name='imdb-list'),
    path('challenge-3', MissingInteger.as_view(), name='missing-integer')
]
