from django.db import models
from django.utils.translation import gettext as _


class IMDB(models.Model):
    tconst = models.CharField(max_length=254)
    primary_title = models.CharField(_('Popular Title'), max_length=254)
    # original_title = models.CharField(_('Original Title'), max_length=254)
    # average_rating = models.DecimalField(_('Average'), max_digits=5, decimal_places=2)
    num_votes = models.IntegerField(_('Votes'))

    class Meta:
        db_table = "imdb"

    def __str__(self):
        return f'{self.tconst} - {self.primary_title}'
