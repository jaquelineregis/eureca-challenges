import re


def get_query(query):
    query = re.sub('\D', ',', query)
    list_query = [int(i) for i in query.split(',') if i.isnumeric()]
    return list_query if len(list_query) != 0 else ''


def findMissingInteger(list_a):
    j = 0
    n = 0
    while(len(list_a) > 0):
        bit = findMissingIntegerBit(list_a, j)
        n |= bit << j
        list_a = new_list(list_a, j, bit)
        j += 1
    return n


def findMissingIntegerBit(list_a, j):
    bits = [(list_a[i] >> j) & 1 for i in range(len(list_a))]
    return 1 if bits.count(1) < bits.count(0) else 0


def new_list(list_a, j, bit):
    return [list_a[i] for i in range(len(list_a)) if ((list_a[i] >> j) & 1) == bit]
